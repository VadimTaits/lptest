var gulp = require('gulp');
var webpack = require('gulp-webpack');
var webpackConfig = require('./webpack.config.js');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');
var postcss = require('gulp-postcss');

var cssTransform = function(css) {
  css.eachDecl(function(decl, i) {
    switch (decl.prop) {
      case 'text-align':
        if (decl.value === 'start') {
          decl.parent.insertBefore(i, {
            prop: 'text-align',
            value: 'left'
          });
        }

        break;

      case 'overflow':
        if (decl.value === 'overlay') {
          decl.parent.insertBefore(i, {
            prop: 'overflow',
            value: 'auto'
          });
        }

        break;

      default:
        break;
    }
  });
};

gulp.task('webpack', function() {
  return gulp.src('./assets/js/jsx/entry.es6')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('./js/'));
});


gulp.task('css', function() {
  gulp.src('./assets/stylus/styles.styl')
    .pipe(stylus({
      compress: true,
      'include css': true
    }))
    .pipe(autoprefixer({
        browsers: ['last 2 versions']
    }))
    .pipe(postcss([cssTransform]))
    .pipe(gulp.dest('./css'));
});


gulp.task('default', ['webpack', 'css'], function () {
  gulp.watch(['./assets/js/**/*.*'], ['webpack']);
  gulp.watch(['./assets/stylus/**/*.*'], ['css']);
});