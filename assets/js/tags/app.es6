riot.tag("app", `
<div class="videos-list">
  <div class="controls">
    <input type="text" oninput="{ search }" />
  </div>

  <div class="videos">
    <div class="video" each="{ item in items }">
      { item.title }
    </div>
  </div>
</div>`,
function(opts) {
  this.setInitialState = () => {
    this.items = [];
    this.loadedPages = 0;
    this.hasMore = true;
    this.currentItemNumber = -1;
    this.loading = false;
    this.query = "";
  };

  this.lastAbortTime = (new Date()).getTime();

  this.setInitialState();

  this.searchRequest = () => {
    if (this.loading || !this.hasMore) {
      return;
    }

    let reqTime = (new Date()).getTime();

    this.update({
      loading: true,
      loadedPages: this.loadedPages + 1
    });

    MovieDB.searchMovie({
      query: this.query,
      page: this.loadedPages
    }, (err, res) => {
      if (reqTime < this.lastAbortTime) {
        return;
      }

      this.update({
        loading: false,
        hasMore: this.loadedPages < res.total_pages,
        items: this.items.concat(res.results)
      });
    });
  };

  this.search = (e) => {
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }

    this.searchTimeout = setTimeout(() => {
      this.lastAbortTime = (new Date()).getTime();

      this.setInitialState();

      this.query = e.target.value;

      this.searchRequest();
    }, 500);
  };
});