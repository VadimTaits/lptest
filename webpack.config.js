var webpack = require('webpack');

module.exports = {
  context: __dirname + '/assets/js',
  entry: './entry.es6',
  output: {
    path: __dirname + '/js',
    filename: './bundle.js'
  },
  module: {
    loaders: [{
      test: /\.es6$/,
      loader: 'babel-loader'
    }, {
      test: /\.json$/,
      loader: 'json-loader'
    }]
  },
  plugins: [
    new webpack.ProvidePlugin({
      riot: 'riot',
      Fluxxor: 'fluxxor',
      mdb: 'moviedb',

      MovieDB: __dirname + '/assets/js/globals/MovieDB.js'
    })
  ]
};